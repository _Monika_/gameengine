#include "Resources.h"
typedef std::map<std::string, sf::Texture*> TextureMap;

std::map<std::string, sf::Texture*> Resources::Textures;
std::map<std::string, Scene*> Resources::Scenes;
std::map<std::string, Character*> Resources::Characters;


sf::Texture* Resources::GetTexture(std::string name)
{
	TextureMap::const_iterator pos = Textures.find(name);
	if (pos == Textures.end()) {
		//handle the error
		Debug::Log("Resources::GetTexture : Texture is not found! \"" + name + "\"", Error);
		return nullptr;
	}
	else {
		return pos->second;
	}
}

bool Resources::UnloadTexture(std::string name)
{
	TextureMap::const_iterator pos = Textures.find(name);
	if (pos == Textures.end()) {
		Debug::Log("Resources::UnloadTexture : Texture is not found! \"" + name + "\"", Error);
		return false;
	}
	else {
		//pos->second->~Texture();
		delete pos->second;
		Textures.erase(name);
		Debug::Log("Resources::UnloadTexture : Texture \"" + name + "\" is unload.", Info);
		return true;
	}
}

void Resources::LoadTexture(std::string path, TextureType type = Other)
{
	path = "D:\\Programming\\C++\\GameEngine\\GameEngine\\Resources\\" + path;
	const size_t last_slash_idx = path.find_last_of("\\/");
	std::string n = path.substr(last_slash_idx + 1);
	n = n.erase(n.size() - 4, n.size());
	
	if (TextureMap::const_iterator cont = Textures.find(n); cont == Textures.end()) {
		Textures.insert(std::pair<std::string, sf::Texture*>(n, new sf::Texture()));
		//Debug::Log(path);
		(*Textures[n]).loadFromFile(path);
	}
	else
		Debug::Log("Resources::LoadTexture : texture \"" + n + "\" is already contains in data base! ", Warning);
}

Scene* Resources::GetScene(std::string name)
{
	return nullptr;
}


bool Resources::LoadScene(std::string path)
{
	path = "D:\\Programming\\C++\\GameEngine\\GameEngine\\Resources\\Scenes\\" + path;
	return false;
}
