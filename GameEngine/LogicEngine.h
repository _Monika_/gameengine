#pragma once
#include <thread>
#include "Debug.h"

class LogicEngine
{
public:
	LogicEngine();
	static LogicEngine* Get();
private:
	std::thread proc;
};

