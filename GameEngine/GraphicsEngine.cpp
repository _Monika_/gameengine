#include "GraphicsEngine.h"
#include "Window.h"


GraphicsEngine::GraphicsEngine()
{

	Debug::Log("Starting graphic engine...", Info);



	Debug::Log("Graphic engine has be started!", Info);
}
/*
int GraphicsEngine::FindTexture(std::string name)
{
	for (size_t t = 0; t < TexturesNames.size(); t++)
		if (TexturesNames[t] == name)
			return t;
	Debug::Log("GraphicsEngine::FindTexture : not found sprite! \"" + name + "\"", Error);
	return -1;
}
*/
void GraphicsEngine::ShowSprite(std::string name, sf::Vector2f pos, size_t id)
{
	if (sf::Texture * tex = Resources::GetTexture(name); tex != NULL) {
		sf::Sprite sp(*tex);
		sp.setPosition(pos.x, pos.y + 120);//120
		sp.scale(1, 1);

		Window::Get()->sprites.push_back(sp);

		ActiveSprites.push_back(id);
		NumberSprites.push_back(Window::Get()->sprites.size());
		Debug::Log("Show!");
	}
}

void GraphicsEngine::ShowdBG(std::string name, size_t id)
{
	if (sf::Texture * tex = Resources::GetTexture(name); tex != NULL) {
		sf::Sprite sp(*tex);
		//sp.setPosition(pos.x, pos.y + 100);//120
		sp.scale(1.2, 1.2);
		
		Window::Get()->background.push_back(sp);
	
		ActiveBG.push_back(id);
		NumberBG.push_back(Window::Get()->background.size());
	}
}

void GraphicsEngine::RemoveGraph(size_t id, TextureType type)
{
}


GraphicsEngine* GraphicsEngine::Get()
{
	static GraphicsEngine* graph = new GraphicsEngine();
	return graph;
}

GraphicsEngine::~GraphicsEngine()
{
}

