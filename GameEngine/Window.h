#pragma once
#include "Debug.h"
#include <SFML/Graphics.hpp>
#include <future>
#include <vector>
#include <iostream>
#include <Windows.h>
#include <thread>
#include "Resources.h"

class Window
{
public:
	Window();
	~Window();

	static sf::Vector2u DisplaySize;

	bool isStart;

	static std::vector<sf::Vector2u> ScreenSizes;
	static std::vector<float> ScreenMultiply;

	std::vector<sf::Sprite> background;
	std::vector<sf::Sprite> sprites;
	std::vector<sf::CircleShape> graphic;

	sf::Vector2u CurrentSize;
	float CurrentMultiply;

	bool SetSize(sf::Vector2u vec);

	static Window* Get();
private:
	std::thread proc;
};

