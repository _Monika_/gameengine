#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Resources.h"

class World : public Resources
{
public:
	static Scene* GetScene();
private:
	static Scene* scene;
};

