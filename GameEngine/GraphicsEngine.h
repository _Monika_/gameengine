#pragma once
#include "Debug.h"
#include <vector>
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Resources.h"

class GraphicsEngine
{
public:
	GraphicsEngine();

	void ShowSprite(std::string name, sf::Vector2f pos, size_t id);
	void ShowdBG(std::string name, size_t id);

	void RemoveGraph(size_t id, TextureType type);

	static GraphicsEngine* Get();

	~GraphicsEngine();
private:
	std::vector<size_t> ActiveSprites; std::vector<size_t> NumberSprites;
	std::vector<size_t> ActiveBG; std::vector<size_t> NumberBG;
};

