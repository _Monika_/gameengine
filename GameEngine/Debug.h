#pragma once
#include <iostream>
#include <string>

enum Type {
	Error, Warning, Info, Undefined
};

class Debug
{
public:
	static void Log(std::string mess, Type type = Undefined);
};

