#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include "Character.h"
#include "Scene.h"
#include "GUI.h"

enum TextureType {
	BG, CG, Sprite, GUI, Other
};

class Resources
{
public:
	static Scene* GetScene(std::string name);
	static sf::Texture* GetTexture(std::string name);

	//int FindTexture(std::string name);
	static void LoadTexture(std::string path, TextureType type);
	static bool LoadScene(std::string path);
	static bool UnloadTexture(std::string name);
private:
	static std::map<std::string, sf::Texture*> Textures;
protected:
	static std::map<std::string, Scene*> Scenes;
	static std::map<std::string, Character*> Characters;
};

