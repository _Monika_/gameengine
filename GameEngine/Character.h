#pragma once
#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include "Debug.h"
#include <vector>
#include <map>

class Emotion
{
public:
	 Emotion();
	~Emotion();
private:

};

class Character : public Emotion
{
public:
	Character();

	bool SetPose(std::string pose);

	std::string Name;
	sf::Vector2f Pos;

	Emotion emotion;
	/*

	*/
protected:
	std::string CurrentPose;
	//std::map<std::string, std::vector<sf::Sprite>> Poses;
};

