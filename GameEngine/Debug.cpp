#include "Debug.h"

void Debug::Log(std::string mess, Type type)
{
	std::string pref;
	switch (type) {
	case Error:
	{
		pref = "[ERROR]";
		break;
	}
	case Warning:
	{
		pref = "[Warning]";
		break;
	}
	case Undefined:
	{
		pref = "[Log]";
		break;
	}
	case Info:
	{
		pref = "[INFO]";
		break;
	}
	};
	std::cout << pref << " " << mess << std::endl;
}
