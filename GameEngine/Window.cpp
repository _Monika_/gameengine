#include "Window.h"

std::vector<sf::Vector2u> Window::ScreenSizes;
std::vector<float> Window::ScreenMultiply;
sf::Vector2u Window::DisplaySize;

Window::Window() {
	Debug::Log("Creating window...", Type::Info);

	int iWidth = GetSystemMetrics(SM_CXSCREEN);  // ���������� ������ �� �����������
	int iHeight = GetSystemMetrics(SM_CYSCREEN); // ���������� ������ �� ���������
	DisplaySize = sf::Vector2u(iWidth, iHeight); //  - 23

	isStart = true; 
	
	graphic.push_back(sf::CircleShape(40, 3));
	
	ScreenSizes = std::vector<sf::Vector2u>{
		sf::Vector2u(640, 360),
		sf::Vector2u(854, 480),
		sf::Vector2u(960 , 540),
		sf::Vector2u(1280, 720),
		sf::Vector2u(1920, 1080),
	};

	CurrentSize = DisplaySize;

	proc = std::thread([] {
		auto& wn = *Window::Get();
		sf::RenderWindow window(sf::VideoMode(wn.CurrentSize.x, wn.CurrentSize.y), "Engine"); // , sf::Style::Fullscreen
		//sf::RenderWindow window(sf::VideoMode(wn.CurrentSize.x, wn.CurrentSize.y), "Engine", sf::Style::Fullscreen); // , sf::Style::Fullscreen
		//window.setPosition(sf::Vector2i(window.getPosition().x, window.getPosition().y));
		//float modifer = 0;
		sf::Vector2u a = window.getSize();

		while (window.isOpen()) {
			Sleep(100);
			if (wn.CurrentSize.x != a.x || wn.CurrentSize.y != a.y) {
				a = wn.CurrentSize;
				window.setSize(a);
				//Debug::Log("fdssd");
			}

			sf::Event event;
			while (window.pollEvent(event)) {
				switch (event.type)
				{
				case sf::Event::Closed: // ������������ ��������� ������� ����: �� ��������� ����
					window.close();
				case sf::Event::Resized:
				{
					double w = static_cast<double>(event.size.width);
					double h = static_cast<double>(event.size.height);
					//if(wn.CurrentSize.y > Window::DisplaySize.y / 2)
					//	modifer = (Window::DisplaySize.y - wn.CurrentSize.y) / 12;
					//else
					//	modifer = (Window::DisplaySize.y - wn.CurrentSize.y) / -12;
					wn.CurrentSize = sf::Vector2u(w, h);
					window.setView(sf::View(
						sf::Vector2f(w / 2.0, h / 2.0),
						sf::Vector2f(w, h)));
					//window.setSize(wn.CurrentSize);
					break;
				}
				default:
					break;
				}
			}

			//std::cout << window.getSize().x << " " << window.getSize().y << std::endl;

			window.clear();
			for(auto& a:wn.graphic)
				window.draw(a);
			for (auto bg : wn.background) {
				bg.scale((float(wn.CurrentSize.x) / float(Window::DisplaySize.x)), (float(wn.CurrentSize.y) / float(Window::DisplaySize.y)));
				window.draw(bg);
			}
			for (auto a : wn.sprites) {
				//a.scale(wn.CurrentMultiply, wn.CurrentMultiply);
				a.scale((float(wn.CurrentSize.x) / float(Window::DisplaySize.x)), (float(wn.CurrentSize.y) / float(Window::DisplaySize.y)));
				window.draw(a);
			}
			window.display();
		}});

	Debug::Log("Window is create!", Type::Info);

}

Window::~Window() {
	isStart = false;
	Sleep(100);
}

bool Window::SetSize(sf::Vector2u vec) {
	this->CurrentSize = vec; //Debug::Log("adsa");
	return true;
}

Window * Window::Get() {
	static Window* window = new Window();
	return window;
}